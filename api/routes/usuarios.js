const express = require('express')
const router = express.Router()
const dados = require('../data/index.js')

router.get('/', (req, res) => {
    res.json(dados.usuarios)
})

// LISTAR APENAS UM
router.get("/:identificador", (req, res) => {
    var id = req.params.identificador

    res.json(dados.usuarios[id])
})

// INSERIR
router.post("/", (req, res) => {
    var novo_usuario = req.body.nome

    dados.usuarios.push(novo_usuario)

    res.json(novo_usuario)
})

// ATUALIZAR
router.put("/:identificador", (req, res) => {
    var novo_usuario = req.body.nome
    var id = req.params.identificador

    dados.usuarios[id] = novo_usuario

    res.json(novo_usuario)
})

// DELETAR
router.delete("/:identificador", (req, res) => {
    var id = req.params.identificador
    var usuario = dados.usuarios[id]
    
    dados.usuarios.splice(id, 1)

    res.json(usuario)
})

module.exports = router