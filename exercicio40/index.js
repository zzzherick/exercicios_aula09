const express = require("express");
const app = express();
const rotaOficinas = require("./api/routes/oficinas");
const rotaLocais = require("./api/routes/locais");
const rotaUsuarios = require("./api/routes/usuarios");

app.use(express.json());

app.use("/oficinas", rotaOficinas);
app.use("/locais", rotaLocais);
app.use("/usuarios", rotaUsuarios);

app.listen(3000, () => {
    console.log('Aplicação executando na porta 3000');
});