const express = require('express')
const app = express()
const cursos = require('./api/routes/cursos')

// MIDDLEWARE
app.use(express.json())

app.use('/cursos', cursos)

app.listen(8080, () => {
    console.log("API Iniciada com sucesso!")
})