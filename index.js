const express = require('express')
const app = express()
const usuarios = require('./api/routes/usuarios')

/*
/// ADICIONA ALGUNS CABEÇÁRIOS NAS REQUISIÇÕES
const cors = require('cors')
app.use(cors())
*/

// MIDDLEWARE
app.use(express.json())

app.use('/users', usuarios)

app.listen(8080, () => {
    console.log("API Iniciada com sucesso!")
})

/// ABAIXO UM CRUD NO SECO ///
/*
var dados = {
    versao: "1.0",
    usuarios: [
        "ramon", "jose", "maria"
    ]
}
*/

/*
// LISTAR
app.get("/usuarios", (req, res) => {
    res.json(dados.usuarios)
})

// LISTAR APENAS UM
app.get("/usuarios/:identificador", (req, res) => {
    var id = req.params.identificador

    res.json(dados.usuarios[id])
})

// INSERIR
app.post("/usuarios", (req, res) => {
    var novo_usuario = req.body.nome

    dados.usuarios.push(novo_usuario)

    res.json(novo_usuario)
})

// ATUALIZAR
app.put("/usuarios/:identificador", (req, res) => {
    var novo_usuario = req.body.nome
    var id = req.params.identificador

    dados.usuarios[id] = novo_usuario

    res.json(novo_usuario)
})

// DELETAR
app.delete("/usuarios/:identificador", (req, res) => {
    var id = req.params.identificador
    var usuario = dados.usuarios[id]
    
    dados.usuarios.splice(id, 1)

    res.json(usuario)
})
*/

/*
app.get("/versao", (req, res) => {
    res.json(dados.versao)
})
*/